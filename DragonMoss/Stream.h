#pragma once
#include <iostream>
#include <utility>

class Stream
{
public:
	template <typename Arg, typename... Args>
	static void print(Arg&& arg, Args&&... args) {
		std::cout << std::forward<Arg>(arg);
		((std::cout << ' ' << std::forward<Args>(args)), ...);
		std::cout << std::endl;
	}
};
