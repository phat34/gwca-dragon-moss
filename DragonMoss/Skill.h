#pragma once
#include <string>
#include <GWCA/stdafx.h>
#include <GWCA/GameContainers/Array.h>
#include <GWCA/Utilities/Export.h>
#include <GWCA/Managers/SkillbarMgr.h>
#include <GWCA/Managers/AgentMgr.h>
class Skill
{
public:
	static void loadSkillTemplate(std::string skillTemplate);
	static void useSkill(int skillSlot, int target);
};

