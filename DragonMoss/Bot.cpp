#include "Bot.h"

Bot::Bot() {}

void Bot::run() {
	Stream::print("Traveling to Saint Anjeka");
	Navigation::travelTo(GW::Constants::MapID::Saint_Anjekas_Shrine_outpost);
	Stream::print("Loading build template");
	Skill::loadSkillTemplate("OgcTcZ88ZiHRn5AqB6u85A3R4AA");
	GW::PartyMgr::SetHardMode(true);
	Stream::print("Moving outside");
	moveOutside();
	Skill::useSkill(5, -2);
	Skill::useSkill(4, -2);

	Stream::print("Moving to farm spot");
	moveTo(-8498, 18548, 50);
	moveTo(-6858, 17713, 50);
	do {
		GW::GameThread::Enqueue([]() {
			GW::Agents::Move(-11172, -23105);
		});
		Sleep(100);
	} while (Navigation::getNumberOfFoesInRangeOfAgent(-2, 1300) > 0);

	Stream::print("Casting enchants");
	Skill::useSkill(1, -2);
	Skill::useSkill(2, -2);
	Skill::useSkill(3, -2);

	Stream::print("Aggroing mobs");
	Skill::useSkill(5, -2);
	moveTo(-5234, 15574, 50);
	Stream::print("Balling Moss");
	moveTo(-6131, 17952, 50);
	Sleep(750);
	moveTo(-6570, 18493, 50);
	Sleep(750);
	Skill::useSkill(5, -2);
	Skill::useSkill(4, -2);
	Skill::useSkill(6, -2);
	Skill::useSkill(2, -2);
	Stream::print("Spiking Moss");
}

void Bot::moveOutside() {
	moveTo(-11272, -22523, 50);
	GW::GameThread::Enqueue([]() {
		GW::Agents::Move(-11172, -23105);
	});
	do { Sleep(100); } while (GW::Map::GetMapID() != GW::Constants::MapID::Drazach_Thicket);
}

bool Bot::moveTo(float x, float y, int Random) {
	int Blocked = 0;
	GW::Agent* Me = GW::Agents::GetPlayer();
	do {
		if (Me->hp <= 0) return false;
		if (GW::Map::GetInstanceType() == GW::Constants::InstanceType::Loading) return false;
		GW::GameThread::Enqueue([x, y, Random]() {
			GW::Agents::Move(x + Utilities::generateRandomInt(Random), y + Utilities::generateRandomInt(Random));
		});
		Sleep(200);
		if (Me->move_x == 0 && Me->move_y == 0) Blocked++;
	} while (GW::GetDistance(GW::Vec2f(Me->pos.x, Me->pos.y), GW::Vec2f(x, y)) > Random * 2.5 || Blocked > 15);
	return true;
}