#pragma once
#include <Windows.h>
#include <random>
#include <chrono>

#include <GWCA/stdafx.h>
#include <GWCA/Utilities/Export.h>
#include <GWCA/Utilities/Scanner.h>

class Utilities
{
public:
	static int generateRandomInt(int upperbound);
	static DWORD getPing();
};

