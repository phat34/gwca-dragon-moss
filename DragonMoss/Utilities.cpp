#include "Utilities.h"

int Utilities::generateRandomInt(int upperBound) {
	unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
	std::default_random_engine gen(seed);
	std::uniform_int_distribution<> dis(0, upperBound);
	return (dis(gen));
}

DWORD Utilities::getPing() {
	static DWORD* ping_address = nullptr;
	if (ping_address == nullptr) {
		BYTE* addr = (BYTE*)GW::Scanner::Find("\x33\xC0\x85\xF6\x0F\x94\xC0\x5E\xC3", "xxxxxxxxx", 16);
		printf("Ping Address = 0x%X\n", *(DWORD*)(addr)+4);
		if (addr) {
			ping_address = *(DWORD**)(addr)+1;
		}
	}
	return *ping_address;
}
