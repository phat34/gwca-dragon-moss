#include <Windows.h>
#include <stdint.h>
#include <assert.h>
#include <stdio.h>

#include <GWCA/GWCA.h>

#include <GWCA/Utilities/Hooker.h>
#include <GWCA/Utilities/Scanner.h>

#include "Bot.h"

FILE* fh;

int main() {
	Bot* bot = new Bot();
	bot->run();
}

DWORD __stdcall ThreadEntry(HMODULE hModule) {
	GW::HookBase::Initialize();
	if (!GW::Initialize()) {
		FreeLibraryAndExitThread(hModule, EXIT_SUCCESS);
		return EXIT_SUCCESS;
	}
	GW::HookBase::EnableHooks();

	main();

	GW::DisableHooks();

	while (GW::HookBase::GetInHookCount())
		Sleep(16);
	Sleep(16);

	GW::Terminate();
	FreeLibraryAndExitThread(hModule, EXIT_SUCCESS);
}

DWORD __stdcall SafeThreadEntry(HMODULE hModule) {
	__try {
		return ThreadEntry(hModule);
	}
	__except (EXCEPTION_CONTINUE_SEARCH) {
		return EXIT_SUCCESS;
	}
}


DWORD WINAPI init(HMODULE hModule) {
	__try {
		AllocConsole();
		freopen_s(&fh, "CONOUT$", "w", stdout);
		freopen_s(&fh, "CONOUT$", "w", stderr);
		SetConsoleTitleA("Debug Console");

		if (*(DWORD*)0x00DE0000 != NULL) {
			MessageBoxA(0, "Please restart Guild Wars and try again.", "Client error detected", 0);
			FreeLibraryAndExitThread(hModule, EXIT_SUCCESS);
		}

		GW::Scanner::Initialize("Gw.exe");
		DWORD **found = (DWORD **)GW::Scanner::Find("\xBA\x01\x00\x00\x00\x3B\xC2\x75\x0A", "xxxxxxxxx", -22);
		if (!(found && *found)) {
			MessageBoxA(0, "No Logged In Character.", "DLL Error", 0);
			FreeLibraryAndExitThread(hModule, EXIT_SUCCESS);
		}
		else {
			printf("[SCAN] is_ingame = %p\n", found);
		}

		DWORD *is_ingame = *found;
		while (*is_ingame == 0) {
			Sleep(100);
		}

		SafeThreadEntry(hModule);

		fclose(fh);
		FreeConsole();
	}
	__except (EXCEPTION_CONTINUE_SEARCH) {
	}
	return 0;
}


BOOL WINAPI DllMain(_In_ HMODULE _HDllHandle, _In_ DWORD _Reason, _In_opt_ LPVOID _Reserved) {
	DisableThreadLibraryCalls(_HDllHandle);
	if (_Reason == DLL_PROCESS_ATTACH) {
		__try {
			CreateThread(0, 0, (LPTHREAD_START_ROUTINE)init, _HDllHandle, 0, 0);
		}
		__except (EXCEPTION_CONTINUE_SEARCH) {
		}
	}
	return TRUE;
}
