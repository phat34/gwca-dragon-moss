#include "Navigation.h"
#include <Windows.h>


Navigation::Navigation()
{
}

void Navigation::travelTo(GW::Constants::MapID mapId)
{
	bool isAlreadyOnMap = GW::Map::GetMapID() == mapId;

	if (!isAlreadyOnMap) {
		GW::Map::Travel(mapId, GW::Constants::District::Current, 0);
		do { Sleep(100); } while (GW::Map::GetMapID() != mapId);
	}
}

int Navigation::getNumberOfFoesInRangeOfAgent(int target, float distance)
{
	uint32_t agentId = 0;
	switch (target) {
		case -1: agentId = GW::Agents::GetTargetId(); break;
		case -2: agentId = GW::Agents::GetPlayerId(); break;
	}
	int count = 0;
	GW::AgentArray agents = GW::Agents::GetAgentArray();
	for (size_t i = 0; i < agents.size(); ++i) {
		GW::Agent* agent = agents[i];
		if (agent == nullptr) continue;
		if (agent->type != 0xDB) continue;
		if (agent->GetIsDead() || agent->hp <= 0) continue;
		if (agent->allegiance != 3) continue;
		if (GW::GetDistance(agent->pos, GW::Agents::GetAgentByID(agentId)->pos) > distance) continue;
		++count;
	}
	return count;
}

int Navigation::getMaxAgents() {
	int compass_count = 0;
	GW::AgentArray agents = GW::Agents::GetAgentArray();
	GW::Agent* player = GW::Agents::GetPlayer();
	if (GW::Map::GetInstanceType() != GW::Constants::InstanceType::Loading
		&& agents.valid()
		&& player != nullptr) {
		for (unsigned int i = 0; i < agents.size(); ++i) {
			GW::Agent* agent = agents[i];
			if (agent == nullptr) continue;
			if (agent->allegiance != 0x3) continue;
			if (agent->GetIsDead()) continue;
			++compass_count;
		}
	}
}
