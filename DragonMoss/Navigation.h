#pragma once
#include <GWCA/stdafx.h>
#include <GWCA/Utilities/Export.h>
#include <GWCA/Constants/Constants.h>
#include <GWCA/GameEntities/Map.h>
#include <GWCA/Managers/MapMgr.h>
#include <GWCA/Managers/AgentMgr.h>
#include <GWCA/GameEntities/Agent.h>
#include <GWCA/GameContainers/GamePos.h>

class Navigation {
public:
	Navigation();
	static void travelTo(GW::Constants::MapID);
	static int getNumberOfFoesInRangeOfAgent(int target, float distance);
	static int getMaxAgents();
};

