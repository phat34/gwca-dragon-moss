#include "Skill.h"

void Skill::loadSkillTemplate(std::string skillTemplate)
{
	GW::SkillbarMgr::LoadSkillTemplate(skillTemplate.c_str());
}

void Skill::useSkill(int skillSlot, int target)
{
	uint32_t agent = 0;
	switch (target) {
		case -1: agent = GW::Agents::GetTargetId(); break;
		case -2: agent = GW::Agents::GetPlayerId(); break;
	}
	GW::SkillbarMgr::UseSkill(skillSlot, agent);
}
