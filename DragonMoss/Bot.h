#pragma once
#include "Navigation.h"
#include "Skill.h"
#include "Utilities.h"
#include "Stream.h"

#include <GWCA/stdafx.h>
#include <GWCA/Utilities/Export.h>

#include <GWCA/Constants/Constants.h>
#include <GWCA/Constants/Maps.h>

#include <GWCA/Managers/GameThreadMgr.h>
#include <GWCA/Managers/PartyMgr.h>
#include <GWCA/Managers/MapMgr.h>
#include <GWCA/Managers/AgentMgr.h>
#include <GWCA/Managers/SkillbarMgr.h>

#include <GWCA/GameContainers/GamePos.h>

#include <GWCA/GameEntities/Agent.h>
#include <GWCA/GameEntities/Player.h>
#include <GWCA/GameEntities/Map.h>
#include <GWCA/GameEntities/Pathing.h>

class Bot
{
public:
	Bot();
	void run();

private:
	// void move(float x, float y, int Random);
	bool moveTo(float x, float y, int Random);
	void moveOutside();

};

