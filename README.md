# GWCA Dragon Moss

## What is it ?
A bot to farm Dragon Moss in Guild Wars using GWCA++.

## How to download, build, and run
1. Clone *recursively* the repository. On command line, use: `git clone --recusrive https://gitlab.com/QuarkyUp/gwca-dragon-moss.git`.
2. Open DragonMoss.sln with Visual Studio. The bot is developed under VS2017 with v141 toolset, but earlier versions should work too.
3. Set to Release x86
4. Build the solution
5. Inject the generated dll file into your Guild Wars client